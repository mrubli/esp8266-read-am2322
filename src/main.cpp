/*
 * AM2322 Temperature/humidity sensor read-out
 *
 * Pin assignment:
 * AM2322   NodeMCU     Wemos D1 look-alike
 * SCL      D1          D3
 * SDA      D2          D4
 */

#include <Arduino.h>
#include <Wire.h>


#define PRINT_RAW_DATA 0


namespace
{

    const auto I2cAddress = 0xB8 >> 1;

}


// From the AM2322 specification
static
unsigned short
crc16(unsigned char *ptr, unsigned char len)
{
    unsigned short crc = 0xFFFF;
    unsigned char i;
    while (len--)
    {
        crc ^= *ptr++;
        for (i = 0; i < 8; i++)
        {
            if (crc & 0x01)
            {
                crc >>= 1;
                crc ^= 0xA001;
            }
            else
            {
                crc >>= 1;
            }
        }
    }
    return crc;
}

void setup()
{
    Serial.begin(9600);

#if 1
    // Initialize I2C with default pin parameters
    Wire.begin();   // Pins 4 (D2 = SDA) and 5 (D1 = SCL)
#else
    // Use this for custom setups (these values are for the NodeMCU board)
    static_assert(D1 == 5, "D1 ouch");
    static_assert(D2 == 4, "D2 ouch");
    static_assert(SDA == D2, "SDA ouch");
    static_assert(SCL == D1, "SCL ouch");
    Wire.begin(SDA, SCL);
#endif
}

void loop()
{
    // The specification gives a minimum sampling period of 2 seconds but in practice reading
    // more often than that seems okay.
    delay(1000);

    // Read temperature and humidity according to the AM2322 specification, section 8.2.4
    //

    // Wake-up
    Wire.beginTransmission(I2cAddress);
    Wire.endTransmission();

    // Start a read for registers 0x00 to 0x03
    {
        Wire.beginTransmission(I2cAddress);
        byte outBuffer[3] = {
            0x03,   // Magic number
            0x00,   // Start address
            0x04,   // Read size
        };
        Wire.write(outBuffer, sizeof(outBuffer));
        const auto res = Wire.endTransmission();
        if(res != 0)
        {
            Serial.print("ERROR: I2C write failed: ");
            Serial.print(res);
            Serial.println();
            return;
        }
    }

    // Wait for 1.5 ms before reading the data
    delayMicroseconds(1500);

    // Read the response data
    byte inBuffer[8] = {};
    {
        Wire.requestFrom(I2cAddress, 4 + 4);
        auto offset = 0u;
#if PRINT_RAW_DATA
        Serial.print("Received:");
#endif
        while(offset < sizeof(inBuffer) && Wire.available() > 0)
        {
            inBuffer[offset] = Wire.read();
#if PRINT_RAW_DATA
            Serial.print(" 0x");
            Serial.print(inBuffer[offset], HEX);
#endif
            offset++;
        }
#if PRINT_RAW_DATA
        Serial.println();
#endif
        if(offset < sizeof(inBuffer))
        {
            Serial.print("ERROR: Short I2C read (");
            Serial.print(offset);
            Serial.println(" bytes, expected: 8 bytes)");
            return;
        }
    }

    // Verify the CRC
    const auto crc = static_cast<uint16_t>((inBuffer[7] << 8) | inBuffer[6]);
    const auto computedCrc = crc16(inBuffer, 6);
    if(crc != computedCrc)
    {
        Serial.println("ERROR: CRC mismatch");
        return;
    }

    // Extract humidity and temperature data
    const auto humidity    = static_cast<int16_t> ((inBuffer[2] << 8) | inBuffer[3]) / 10.0f;   // [% RH]
    const auto temperature = static_cast<int16_t> ((inBuffer[4] << 8) | inBuffer[5]) / 10.0f;   // [℃]

    Serial.print("Temp: ");
    Serial.print(temperature, 2);
    Serial.print(", Hum: ");
    Serial.print(humidity, 2);
#if PRINT_RAW_DATA
    Serial.print(", CRC (Sensor): ");
    Serial.print(crc, HEX);
    Serial.print(", CRC (Host): ");
    Serial.print(computedCrc, HEX);
#endif
    Serial.println();
}
